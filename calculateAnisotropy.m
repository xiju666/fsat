%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the anisotropy of several roughness parameters 
% of a surface (spatial, amplitude, JRC, Hurst) for several angles based on
% the user input of .angleStep. The function first interpolates the 
% relative height along the designated profile and then determines the
% derivation from the mean line for each individual profile. For those
% values, the usual roughness analysis for a profile is conducted.
% As outputs, this function saves the interpolated profiles as .csv files
% called anisotropy_profile_XXdeg.csv as well as images of the profiles.
% The calculated roughness parameters are saved in a anisotropy.csv file
% sorted by the different degree of rotation to the horizontal.
% The determined values are also visualized and saved in figures using
% polar coordinates.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function calculateAnisotropy(surface)

  % Generate list of angles to be tested based on user input
  ListAngles = 0:surface.angleStep:179.9;
  
  % Iterate through each angle to be tested
  profile = surface;
  profile.figures = "no";
  profile.pathResults = strcat(surface.pathResults,'/Anisotropy/');
  if ~exist(profile.pathResults, 'dir')
    mkdir(profile.pathResults);
  end
  for i=1:length(ListAngles)
    
    % Find y-values (devFromMean) for a line rotated by this angle to the 
    % horizontal that goes through the mid-point of the surface
    if (ListAngles(i) <= 45.0 || ListAngles(i) >= 135.0)
      x = min(surface.xUniform,[],2):surface.spacing:...
        max(surface.xUniform,[],2);
      y = tand(ListAngles(i)) .* x + ...
      ( floor( 0.5 * size(surface.relHeight,1) ) - ...
      tand(ListAngles(i)) * floor( 0.5 * size(surface.relHeight,2) ) ) ...
      * surface.spacing;
      profile.spacing = abs( ( x(2) - x(1) ) / cosd(ListAngles(i)) );
    % 90 Degrees require special handling!
    elseif (ListAngles(i) > 89.9 && ListAngles(i) < 90.1)
      y = min(surface.yUniform,[],1):surface.spacing:...
        max(surface.yUniform,[],1);
      x = floor( 0.5 * size(surface.relHeight,2) ) * surface.spacing ...
        .* ones(1,length(y));
      profile.spacing = surface.spacing;
    else
      y = min(surface.yUniform,[],1):surface.spacing:...
        max(surface.yUniform,[],1);
      x = ( y - ( floor( 0.5 * size(surface.relHeight,1) ) - ...
        tand(ListAngles(i)) * floor( 0.5 * size(surface.relHeight,2) ) )...
        * surface.spacing ) ./ tand(ListAngles(i)) ;
      profile.spacing = abs( ( x(2) - x(1) ) / cosd(ListAngles(i)) );
    end
    profile.xUniform = profile.spacing .* (0:1:length(x)-1)';
    
    profile.relHeight = interp2(surface.xUniform, surface.yUniform, ...
      surface.relHeight, x,y)';
    
    profile.meanHeight = mean(profile.relHeight,'omitnan');
    
    profile.devFromMean = profile.relHeight - profile.meanHeight;

    if (strcmp(surface.figures,'yes'))
      figure()
      plot(profile.xUniform,profile.devFromMean)
      axis tight
      xlabel(['x-direction [', surface.units,']'])
      ylabel(['relative height [', surface.units,']'])
      set(gcf, 'Color', 'w');
      set(gcf, 'Position', [400 400 750 550]);
      exportgraphics(gca,[profile.pathResults,'anisotropy_profile_',...
        num2str(ListAngles(i)),'deg.', ...
        surface.figfileformat], 'Resolution',300)
    end
    
    writematrix([profile.xUniform, profile.devFromMean],...
      [profile.pathResults,'anisotropy_profile_',...
        num2str(ListAngles(i)),'deg.csv']);
        
    % calculate spatial and amplitude parameters for this profile
    spatialAmplitude(i) = calculateSpatialAmplitude(profile,1);

    %% Joint Rougness Coefficient
    jrc(i) = calculateJRC(profile,1);

    %% Fractal Dimension
    hurst(i) = calculateHurstExp(profile, 1);
  end

  %% Generate Rose diagrams
  close all
  if (strcmp(surface.figures,'yes'))
    Radians = (0:surface.angleStep:360) .* (2.0*pi/360.0);
    
    for i=1:length(ListAngles)
      Value(i) = spatialAmplitude(i).meanRt;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca,[profile.pathResults,'anisotropy_meanRt.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = spatialAmplitude(i).meanRp;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca,[profile.pathResults,'anisotropy_meanRp.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = spatialAmplitude(i).meanRv;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca,[profile.pathResults,'anisotropy_meanRv.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = abs(spatialAmplitude(i).meanRz);
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,'anisotropy_ABS(meanRz).',...
      profile.figfileformat], 'Resolution',300')
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = abs(spatialAmplitude(i).meanMs);
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,'anisotropy_ABS(meanMs).',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = spatialAmplitude(i).meanThetap;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,'anisotropy_meanThetap.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = abs(spatialAmplitude(i).meanMc);
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca,[profile.pathResults,'anisotropy_ABS(meanMc).',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = spatialAmplitude(i).meanNp;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca,[profile.pathResults,'anisotropy_meanNp.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = spatialAmplitude(i).meanN0;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,'anisotropy_meanN0.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = jrc(i).meanJRC;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,'anisotropy_meanJRC.',...
      profile.figfileformat], 'Resolution',300)
    
    Value=[];
    for i=1:length(ListAngles)
      Value(i) = hurst(i).meanHurst;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,'anisotropy_meanHurst.',...
      profile.figfileformat], 'Resolution',300)

    Value=[];
    for i=1:length(ListAngles)
      Value(i) = hurst(i).meanAHurst;
    end
    Value = [Value Value Value(1)];
    figure()
    polarplot(Radians,Value)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [profile.pathResults,...
      'anisotropy_meanAmplitudeHurst.',...
      profile.figfileformat], 'Resolution',300)
  end
  
  %% Generate output
  G = struct2table(spatialAmplitude);
  J = struct2table(jrc);
  H = struct2table(hurst);

  results = [G,J,H];
  
  rownames = arrayfun(@num2str, ListAngles, 'UniformOutput', false);
  for i=1:length(rownames)
    rownames(i) = strcat(rownames(i),'deg');
  end
  
  results.Properties.RowNames=rownames;

  % export table
  writetable(results,strcat(profile.pathResults,'Anisotropy.csv'),...
    'WriteRowNames',true);

end