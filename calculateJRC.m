%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the Z2 and joint roughness coefficient (JRC) 
% values for an arbitrary number of 1D profiles. When passed more than one
% profile, the function further calculates the statistical distribution of
% the JRC values for all profiles. When passed more than one profile, the
% function generates two figures: The boxplot of the distribution of the
% JRC as well as the two profile lines corresponding to the highest and
% lowest JRC value of the distribution passed.
% The name of the generated plots are:
% - dimX_JRC_boxplot
% - dimX_JRC_min_max
% with X=1,2 depending on the passed value of dim.
%
% The equations in this function are based on:
% Tse, R., & Cruden, D. M. (1979). Estimating joint roughness coefficients.
% International Journal of Rock Mechanics and Mining Sciences & 
% Geomechanics Abstracts, 16(5), 303–307. 
% https://doi.org/10.1016/0148-9062(79)90241-9
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [result] = calculateJRC(structure,dim)

  z = structure.devFromMean;
  spacing = structure.spacing;

  % Calculate Z2 value (Tse & Cruden, 1979; equ. 10)
  Z2 = sqrt ( 1.0 ./ (sum(~isnan(z),dim)-1.0) ./ spacing^2 .* ...
      sum(diff(z,1,dim).^2,dim,'omitnan') );

  % Calculates the JRC value (Tse & Cruden, 1979; equ. 11)
  JRC = 32.2 + 32.47 * log10(Z2(Z2>0));

  % Calculate statistics if more than 1 profile is given
  result.meanZ2 = mean(Z2(Z2>0),'omitnan');
  result.minZ2 = min(Z2(Z2>0),[],'omitnan');
  result.maxZ2 = max(Z2(Z2>0),[],'omitnan');
  result.stdZ2 = std(Z2(Z2>0),[],'omitnan');
  result.meanJRC = mean(JRC,'omitnan');
  result.minJRC = min(JRC,[],'omitnan');
  result.maxJRC = max(JRC,[],'omitnan');
  result.stdJRC = std(JRC,[],'omitnan');

  % Generates figures
  if (strcmp(structure.figures,'yes') && max(size(JRC)) > 1)
    figure()
    idx = ~isnan(JRC);
    boxplot(JRC(idx),'label',' ')
    ylabel('JRC');
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 350 550]);
    exportgraphics(gca, [structure.pathResults,'dim',num2str(dim),...
        '_JRC_boxplot.',structure.figfileformat], 'Resolution', 300)

    figure()
    [M1,I1] = min(JRC,[],'omitnan');
    [M2,I2] = max(JRC,[],'omitnan');
    if (dim==1)
      plot(0:spacing:spacing*(length(z(:,I1))-1),z(:,I1)-min(z(:,I1)),...
        '-r');
      hold on
      plot(0:spacing:spacing*(length(z(:,I2))-1),z(:,I2)-min(z(:,I2)),...
        '-k');
      xlim([0 spacing*length(z(:,I1))])
    else
      plot(0:spacing:spacing*(length(z(I1,:))-1),z(I1,:)-min(z(I1,:)),...
        '-r');
      hold on
      plot(0:spacing:spacing*(length(z(I2,:))-1),z(I2,:)-min(z(I2,:)),...
        '-k');
      xlim([0 spacing*length(z(I1,:))])
    end
    legend(['JRC = ',num2str(M1)], ['JRC = ',num2str(M2)]);
    ylabel(['Height [', structure.units,']'])
    xlabel(['Distance along profile [', structure.units,']'])
    hold off
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'dim',num2str(dim),...
      '_JRC_min_max.',structure.figfileformat], 'Resolution', 300)
  end
end
