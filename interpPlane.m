%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function interpolates a plane (2D surface) or a line (1d profile)
% to the provided raw data. This line / plane is regularly spaced as 
% provided through the metadata field spacing. The interpolated plane / 
% line is for subtracting macroscopic tortuosity that might occur. Do not
% mix this plane with the mean line computed for each profile later on in
% the processing chain. The rawdata as well as the interpolated line /
% plane is saved as a figure called interpolatedPlane.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function zq = interpPlane(structure)
  
  % check if the data is a 2D surface with height values or a 1D profile.
  % Interpolate a plane or a line through the datapoints
  if (size(structure.data,2)>2)
    B = [structure.data(:,1) structure.data(:,2) ...
        ones(size(structure.data(:,1)))] \ structure.data(:,3);
    zq = reshape([structure.xUniform(:), structure.yUniform(:), ...
         ones(size(structure.xUniform(:)))] * B, ...
         numel(structure.yUniform(:,1)), []);
  else
    B = polyfit(structure.data(:,1), structure.data(:,2), 1);
    zq = polyval(B, structure.xUniform);
  end

  if (strcmp(structure.figures,'yes'))
    figure()
    if (size(structure.data,2)>2)
      scatter3(structure.data(:,1),structure.data(:,2),...
          structure.data(:,3))
      hold on
      mesh(structure.xUniform,structure.yUniform,zq)
      hold off
      axis equal;
      cb = colorbar();
      axis tight
      xlabel(['x-direction [', structure.units,']'])
      ylabel(['y-direction [', structure.units,']'])
      cb.Label.String = strcat('height [', structure.units,']');
      cb.Label.FontSize = 12;
    else
      plot(structure.xUniform,zq,'-k')
      hold on
      plot(structure.data(:,1), structure.data(:,2),'--k')
      hold off
      legend('interp. data','raw data')
      axis tight
      xlabel(['x-direction [', structure.units,']'])
      ylabel(['height [', structure.units,']'])
    end
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'interpolatedPlane.',...
      structure.figfileformat], 'Resolution', 300)
  end

end
