%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function is a very simple checker if all required metadata is
% provided in the corrsponding file. Input of metadata is not checked for
% correctness or applicability. Please refer to matlab error messages, if a
% run is not successful.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function error = checkMetadata(sample)
  error = 0;
  for i = 1:1:length(sample)
    disp(['Checking if all required metadata is provided for sample ',...
           num2str(i),'.'])
    if (isfield(sample(i),'case'))
      if (~isfield(sample(i),'name'))
        disp('The field sample(i).name is not provided.')
        error = 1;
      end

      if (isfield(sample(i),'figures'))
        if (~strcmp(sample(i).figures,'yes') && ~strcmp(sample(i).figures, 'no')) 
          disp(['The field .figures is not properly assigned. ' ...
              'Please choose yes or no.'])
          error = 1;
        end
        if (strcmp(sample(i).figures,'yes'))
          if (~isfield(sample(i),'figfileformat'))
            disp('The field .figfileformat is not provided.')
          end
        end
      else
        disp('The field .figures is not provided.')
        error = 1;
      end
        
      if (~isfield(sample(i),'pathResults'))
        disp('The field .pathResults is not provided.')
        error = 1;
      end
        
      if (~isfield(sample(i),'delimiter'))
        disp('The field .delimmiter is not provided.')
        error = 1;
      end
        
      if (~isfield(sample(i),'units'))
        disp('The field .units is not provided.')
        error = 1;
      end
        
      if (~isfield(sample(i),'HurstStart'))
        disp('The field .HurstStart is not provided.')
        error = 1;
      end
        
      if (~isfield(sample(i),'HurstEnd'))
        disp('The field .HurstEnd is not provided.')
        error = 1;
      end
        
      switch sample(i).case
        case 'a'
          disp(['Sample ', num2str(i), ' is a 1d profile.'])
          if (~isfield(sample(i),'pathRawdata'))
            disp('The field pathRawData is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffx0'))
            disp('The field cutoffx0 is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffxend'))
            disp('The field cutoffxend is not provided.')
            error = 1;
          end
        case 'b'
          disp(['Sample ', num2str(i), ' is a 2d surface.'])
          if (~isfield(sample(i),'pathRawdata'))
            disp('The field pathRawData is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffx0'))
            disp('The field cutoffx0 is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffxend'))
            disp('The field cutoffxend is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffy0'))
            disp('The field cutoffy0 is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffyend'))
            disp('The field cutoffyend is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'rotAngle'))
            disp('The field rotAngle is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'spacing'))
            disp('The field spacing is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'angleStep'))
            disp('The field angleStep is not provided.')
            error = 1;
          end
        case 'c'
          disp(['Sample ', num2str(i), ' is a 2d fracture.'])
          if (~isfield(sample(i),'pathRawdataTop'))
            disp('The field pathRawdataTop is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'pathRawdataBottom'))
            disp('The field pathRawdataBottom is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffx0'))
            disp('The field cutoffx0 is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffxend'))
            disp('The field cutoffxend is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'aperture'))
            disp('The field aperture is not provided.')
            error = 1;
          end
        case 'd'
          disp(['Sample ', num2str(i), ' is a 3d fracture.'])
          if (~isfield(sample(i),'pathRawdataTop'))
            disp('The field pathRawdataTop is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'pathRawdataBottom'))
            disp('The field pathRawdataBottom is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffx0'))
            disp('The field cutoffx0 is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffxend'))
            disp('The field cutoffxend is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffy0'))
            disp('The field cutoffy0 is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'cutoffyend'))
            disp('The field cutoffyend is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'rotAngleTop'))
            disp('The field rotAngle is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'rotAngleBottom'))
            disp('The field rotAngle is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'spacing'))
            disp('The field spacing is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'angleStep'))
            disp('The field angleStep is not provided.')
            error = 1;
          end
          if (~isfield(sample(i),'aperture'))
            disp('The field aperture is not provided.')
            error = 1;
          end
        otherwise
          disp('No proper value (a-d) assigned to sample(i).case.')
      end
    else
      disp('The field sample(i).case is not provided.')
    end
  end
end
