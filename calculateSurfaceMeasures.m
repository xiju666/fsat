%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates surface measures to represent roughness on a
% general, surface-wide level instead of spliting the surface into 1D
% profiles. The values calculated here follow the concept presented in:
% Belem, T., Homand-Etienne, F., & Souley, M. (2000). Quantitative 
% Parameters for Rock Joint Surface Roughness. Rock Mechanics and Rock 
% Engineering, 33(4), 217–242. https://doi.org/10.1007/s006030070001
%
% as well as in
%
% Grasselli, G., Wirth, J., & Egger, P. (2002). Quantitative 
% three-dimensional description of a rough surface and parameter evolution 
% with shearing. International Journal of Rock Mechanics and Mining 
% Sciences, 39(6), 789–800. https://doi.org/10.1016/S1365-1609(02)00070-9
%
% with the update in 
%
% Tatone, B. S. A., & Grasselli, G. (2009). A method to evaluate the 
% three-dimensional roughness of fracture surfaces in brittle geomaterials.
% Review of Scientific Instruments, 80(12). 
% https://doi.org/10.1063/1.3266964
%
% and the formulas taked from
%
% Song, L., Jiang, Q., Li, L. fu, Liu, C., Liu, X. pei, & Xiong, J. (2020).
% An enhanced index for evaluating natural joint roughness considering 
% multiple morphological factors affecting the shear behavior. Bulletin of 
% Engineering Geology and the Environment, 79(4), 2037–2057. 
% https://doi.org/10.1007/s10064-019-01700-1
%
% As delauney triangulation and subsequent calculations can be
% computationally demanding with respect to computational power and memory,
% temporarily generated variables are cleared from the workspace as soon as
% possible. Depending on the number of grid points of the used surface,
% computation can still be cumbersome on low-end computers.
% The surface roughness parameter introduced by Grasselli et al. (2002)
% requires the installation of the Matlab curve fitting toolbox. If it is
% not installed, the analysis will not be excuded. If installed, the
% surface roughness coefficient will be calculated following the model
% introduced in Grasselli et al. (2002), improved in Tatone & Grasselli
% (2010) with the equations given in Song et al. (2020). The anisotropy of
% the roughness measure will be evaluated with a similar angular stepsize
% as the general anisotropy analysis (calculateAnisotropy) and as provided
% by the user input angleStep. The analysis based on Grasselli et al.
% (2002) will generate output such as the curve fitting result for each
% test direction as well as the anisotropy in a polar plot.
% 
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [result] = calculateSurfaceMeasures(surface)

  %% Calculate Surface Angularity (Belem et al., 2000)
    z = surface.relHeight;
  % Non-NaN values
  nnanZ = ~isnan(surface.relHeight(:));
  
  % Delaunay Triangulation in 2d because it way more accurate than in 3d
  DT = delaunay(surface.xUniform(nnanZ), surface.yUniform(nnanZ));
  % get the points matching to the triangles
  P = [surface.xUniform(nnanZ), surface.yUniform(nnanZ), ...
    surface.relHeight(nnanZ)];
  % 2D triangulation of surface
  TR = triangulation(DT,P);
  % Compute normal vector of each facet
  F = faceNormal(TR);
  % Normal vector for horizontal x-y plane
  zn = [0 0 1];
  cosTheta = zeros(length(F),1);
  ThetaInDegrees = zeros(length(F),1);
  for i=1:length(F)
    cosTheta(i) = max(min(dot(F(i,:),zn)/(norm(F(i,:))*norm(zn)),1),-1);
    ThetaInDegrees(i) = real(acosd(cosTheta(i)));
  end

  % calculate surface angularity (Belem et al., 2000; equ. 10)
  result.SurfaceAngularity = mean(ThetaInDegrees);
    
  %% Calculate surface Roughness Coeff (Belem et al., 2000)
  % calculate the vectors
  v1 = P(DT(:,2), :) - P(DT(:,1), :);
  v2 = P(DT(:,3), :) - P(DT(:,2), :);
  % calculate the area
  cp = 0.5*cross(v1,v2);
  areaOfTriangles = sqrt(dot(cp, cp, 2));
  surfaceAreaRough = sum(areaOfTriangles);
  surfaceAreaSmooth = surface.spacing^2 * (sum(nnanZ)-size(z,1)-size(z,2));

  % calculate surface roughness coefficient (Belem et al., 2000, equ. 13)
  result.SurfRoughCoeff = surfaceAreaRough / surfaceAreaSmooth;

  clear v1 v2 P cp
    
  %% Calculate surface tortuosity
  % define extreme points
  X = [surface.xUniform(1,1) surface.xUniform(end,end) ...
       surface.xUniform(end,end) surface.xUniform(1,1)];
  Y = [surface.yUniform(1,1) surface.yUniform(1,1) ...
       surface.yUniform(end,end) surface.yUniform(end,end)];
  Z = [surface.relHeight(1,1) surface.relHeight(end,1) ...
       surface.relHeight(end,end) surface.relHeight(1,end)];

  % same as above
  tri = delaunay(X,Y);
  P = [X(:),Y(:),Z(:)];
  v1 = P(tri(:,2), :) - P(tri(:,1), :);
  v2 = P(tri(:,3), :) - P(tri(:,2), :);
  cp = 0.5*cross(v1,v2);
  surfaceAreaCorners = sum(sqrt(dot(cp, cp, 2)));

  % calculate surface tortuosity (Belem et al., 2000; equ. 18)
  % In opposite to the idea in Belem et al. (2000), the real pi-surface is 
  % calculated and not an approximate using least square fit.
  result.surfTort = surfaceAreaRough / surfaceAreaCorners;

  clear X Y Z tri P v1 v2 cp surfaceAreaCorners
    
  %% Z2 Value of whole surface (Belem et al., 2000; equ. 12)
  % determine maximum number of points along the profile
  L1 = sum(~isnan(z),1);
  
  result.Z2s = sqrt ( 1.0 / ( (sum(L1)-size(z,1)-size(z,2)) * ...
         surface.spacing^2 ) * ...
      ( sum(sum( diff(z,1,1).^2, 1, 'omitnan')) + ...
        sum(sum( diff(z,1,2).^2, 2, 'omitnan')) ) );

  clear L1
  %% local max density
  result.Nps = sum(sum(islocalmax(surface.relHeight),'omitnan')) / ...
    surfaceAreaSmooth;
  
  %% Roughness parameter based on Grasseli et al. 2002
  % The fitting requires the curve fitting toolbox of Matlab
  if license('test','curve_fitting_toolbox')
    
    % Projecting the normal vector on the horizontal
    Ni0 = [F(:,1) F(:,2) zeros(length(F(:,1)),1)];
    
    % Shear directions to be considered
    angleShearing = [0:surface.angleStep:359.9, 360];
    
    % Allocating arrays
    cosAlpha = zeros(length(F),1);
    tanThetaStar = zeros(length(F),1);
    listRoughParam = zeros(1,length(angleShearing));
    
    % Loop over shear vectors
    for j = 1:length(angleShearing)-1
      Azero=0.0;
      
      shearVec = [cosd(angleShearing(j)) sind(angleShearing(j)) 0];
      
      for i=1:length(F)
        % Azimuth
        cosAlpha(i) = max(min(dot(Ni0(i,:),shearVec(:))/...
          (norm(Ni0(i,:))*norm(shearVec(:))),1),-1);
        
        % Apparent inclination w.r.t. the shear direction
        tanThetaStar(i) = -cosAlpha(i)*tand(ThetaInDegrees(i));
        
        % Calculate maximum area of active elements in shear direction
        if tanThetaStar(i) > 0
          Azero = Azero + areaOfTriangles(i);
        end
      end
      
      % Maximum apparent inclination
      thetaStarMax = max(atand(tanThetaStar));
      
      % Select stepsize of thetastar and calculate active areas
      anglelist = [0:floor(thetaStarMax/10):thetaStarMax-1,thetaStarMax];
      
      % Allocate array
      areaThetaStar = zeros(length(anglelist),1);
      areaThetaStar(1) = Azero;
      
      for k=2:length(anglelist)
        for i=1:length(tanThetaStar)
          if atand(tanThetaStar(i)) > anglelist(k)
            areaThetaStar(k) = areaThetaStar(k) + areaOfTriangles(i);
          end
        end
      end
      
      % Normalize values
      Azero = Azero ./ surfaceAreaRough;
      areaThetaStar = areaThetaStar ./ surfaceAreaRough;
      
      % Start fitting
      funct = @(C,x) Azero .* ((thetaStarMax - x)./thetaStarMax).^C;
      fitC = fit(anglelist', areaThetaStar, funct,'StartPoint',3);
      
      listRoughParam(j) = thetaStarMax / (fitC.C + 1);

      figure()
      plot(anglelist, areaThetaStar,'*')
      hold on
      plot(fitC, anglelist, areaThetaStar)
      hold off
      xlabel('Apparant dip [°]')
      ylabel('Normalized area [-]')
      set(gcf, 'Color', 'w');
      set(gcf, 'Position', [400 400 750 550]);
      exportgraphics(gca, [surface.pathResults,'SurfaceRoughnessParam_',...
        num2str(angleShearing(j)),'deg_fit.',...
        surface.figfileformat], 'Resolution', 300)
      
    end
    
    listRoughParam(end) = listRoughParam(1);
    result.SurfRoughParam = mean(listRoughParam,'omitnan');
    DiagramRadians= angleShearing.*2.*pi./360.0;
    figure()
    polarplot(DiagramRadians,listRoughParam)
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [surface.pathResults,'SurfaceRoughnessParam_Anisotropy.',...
      surface.figfileformat], 'Resolution', 300)
    
  else
    disp( strcat('The determination of the roughness coefficient as',...
      ' proposed by Grasselli et al. 2002 requires the Curve Fitting',...
      'Toolbox'));
  end
end
