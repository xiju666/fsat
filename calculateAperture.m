%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function reassembles the fracture halves top and bottom to calculate
% the space between both halves for a given mean aperture. 
% If the given mean aperture is NaN (not a number), then the aperture will
% be calculated for the first contact of both sides.
% This function generates 2 figures: A histogram for the statistical
% distribution of the aperture (aperture_histogram) and a plot of the
% spatial distribution of the aperture (aperture_distribution). This
% function further generates a csv file with mean, max, min, standard
% deviation of the aperture and the contact area (aperture.csv). For
% further analysis, such as numerical simulation, the aperture distribution
% and the calculated values are saved in a mat file (aperture.mat).
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function calculateAperture(input)

  %% load data from both sides from workspace files
  load([input.pathResults,'top/workspace.mat'], 'structure');

  topRelHeight = structure.relHeight;

  clearvars -except topRelHeight input

  load([input.pathResults,'bottom/workspace.mat'], 'structure');

  bottomRelHeight = structure.relHeight;

  xq = structure.xUniform;
  if (min(size(structure.data))> 2)
    yq = structure.yUniform;
  end
  zq = structure.zPlane;

  clearvars -except input topRelHeight bottomRelHeight xq yq zq

  %% Arrange so that both sides match and allign to the same plane
  topRelHeight = - flip(topRelHeight, 2);
  sidebottom = zq + bottomRelHeight;
  sidetop = zq + topRelHeight;

  %% Shift top relative to bottom to match given aperture
  if (~isnan(input.aperture))
    shift = input.aperture;

    shiftedtop = sidetop + shift;

    diff = shiftedtop - sidebottom;
    shiftedtop(diff<0) = sidebottom(diff<0);
    diff = shiftedtop - sidebottom;

    iter=0;
    while ( (mean(mean(diff)) < 0.97*input.aperture || ...
             mean(mean(diff)) > 1.03*input.aperture ) && ...
             iter < 1000)
      if (mean(mean(diff)) > 1.03*input.aperture)
        shift = shift + 0.5 * ( input.aperture - mean(mean(diff)) );
      else
        shift = shift + 0.5 * ( input.aperture - mean(mean(diff)) );
      end
      shiftedtop = sidetop + shift;

      diff = shiftedtop - sidebottom;
      shiftedtop(diff<0) = sidebottom(diff<0);
      diff = shiftedtop - sidebottom;
      iter = iter + 1;
    end
  else
    diff = sidetop - sidebottom;
    shiftedtop = sidetop + abs(min(min(diff)));
    diff = shiftedtop - sidebottom;
  end

  %% Calculate statistics and output
  results.meanAperture = mean(diff(:));
  results.minAperture = min(diff(:));
  results.maxAperture = max(diff(:));
  results.stdAperture = std(diff(:));
  results.contactArea = numel(diff) - nnz(diff) * (input.spacing^2);
  results.fractureVolume = sum(diff(:)) * (input.spacing^2);

  resTab = struct2table(results);
  writetable(resTab,strcat(input.pathResults,'aperture.csv'));

  %% Generate figures
  if (strcmp(input.figures,'yes'))
    figure()
    histogram(diff, 100)
    xlabel(['Aperture [', input.units,']'])
    ylabel('Occurrence')
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [input.pathResults,'aperture_histogram.',...
      input.figfileformat],'Resolution', 300)

    figure()
    if (min(size(diff))>1)
      pcolor(xq,yq,diff)
      shading interp
      cb = colorbar();
      axis equal
      axis tight
      xlabel(['x-direction [', input.units,']'])
      ylabel(['y-direction [', input.units,']'])
      cb.Label.String = strcat('aperture [', input.units,']');
      cb.Label.FontSize = 12;
    else
      plot(xq,diff,'-r','LineWidth',1)
      hold on
      plot(xq,shiftedtop,'-k')
      plot(xq,sidebottom,'--k')
      hold off
      axis tight
      legend('aperture','top','bottom')
      ylabel(['aperture / height [', input.units,']'])
      xlabel(['distance along profile [', input.units,']'])
    end
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [input.pathResults,'aperture_distribution.',...
      input.figfileformat],'Resolution', 300)
  end

  %% save workspace
  if exist('yq','var')
    save([input.pathResults,'aperture.mat'],'xq','yq','diff','results')
  else
    save([input.pathResults,'aperture.mat'],'xq','diff','results')
  end

end
