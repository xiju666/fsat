%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function sets the smallest values of each dataset to zero for a
% unified plotting and analysis.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function data = setToZero(structure)

  %% Bring to same groundlevel
  NewXZero = min(structure.data(:,1));
  data(:,1) = structure.data(:,1) - NewXZero;
  NewYZero = min(structure.data(:,2));
  data(:,2) = structure.data(:,2) - NewYZero;
  clear NewXZero NewYZero

  ls = size(structure.data);
  if (ls(2) > 2)
      NewZZero = min(structure.data(:,3));
      data(:,3) = structure.data(:,3) - NewZZero;
      clear NewZZero
  end
  
  if (strcmp(structure.figures,'yes'))
    figure()
    if (strcmp(structure.case,'b') || strcmp(structure.case,'d'))
      scatter(data(:,1), data(:,2),1,data(:,3))
      cb = colorbar();
      axis equal
      axis tight
      xlabel(['x-direction [', structure.units,']'])
      ylabel(['y-direction [', structure.units,']'])
      cb.Label.String = strcat('height [', structure.units,']');
      cb.Label.FontSize = 12;
    else
      plot(data(:,1), data(:,2),'-k')
      axis tight
      xlabel(['x-direction [', structure.units,']'])
      ylabel(['z-direction [', structure.units,']'])
    end
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'rawdata_set_to_zero.',...
      structure.figfileformat], 'Resolution', 300)
  end
  close all
end
