%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This toolbox analysises the surface rougness of one or more sample(s).
% A sample can either consist of
% a) a single profile line (x-z data)
% b) a singe surface (x-y-z data)
% c) two profile lines forming a vertical cut through a fracture (2 x-z
% datasets)
% d) two surfaces forming a three-dimensional fracture based on xyz files
% of each of the 2d surface scans (2 x-y-z datasets)
%
% A batch of samples can be calculated at once. Depending on the sample 
% type various meta-information need to be provided. See the skript 
% "generateMetadata.m" for more information about the required metadata.
%
% Results in form of images and csv files are automatically saved in a
% results folder one depth up in the tree. One can decide if images are
% supposed to be generated or not.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start of Program with a fresh start
clear all
close all
clc

%% User input
% Give filename (and path to file) where metadata is stored
metadatafilename = 'testdata.mat';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% No user input required below this point
%% Load metadata
load(metadatafilename)
clear metadatafilename;

%% Check if all metadata is provided for the chosen case
error = checkMetadata(sample);
if (error > 0)
  disp('I can not proceed! Please check the metadata.')
  return;
else
  disp('Check successfull!')
end

clear error;

%% Start the analysis
for i = 1:1:length(sample)
  disp(['Start calculating roughness of sample ',num2str(i)])
  if ~exist(sample(i).pathResults, 'dir')
    mkdir(sample(i).pathResults);
  end
    
  switch sample(i).case
    case 'a'
      roughnessProfile(sample(i));
      clear ans
    case 'b'
      roughnessSurface(sample(i));
      clear ans
    case 'c'
      roughnessFracture2d(sample(i));
    case 'd'
      roughnessFracture3d(sample(i));
    otherwise
      disp('Error occured while reading sample(i).case.')
  end
  close all
end
