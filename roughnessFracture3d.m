%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function splits the metadata provided for the two surfaces which 
% form top and bottom of the fracture and passes it on for separate
% analysis. The results of each surface are combined and mean values
% as well as divergences are calculated and stored in a csv file in the
% corresponding directory. Additionally to the analysis of the single
% surfaces, the aperture distribution of the fracture is analyzed.
% For future analysis and modeling the workspace is stored as a mat-file.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function roughnessFracture3d(structure)
  
  resultspath = structure.pathResults;

  %% Top
  % overwrite fields for top side
  structure.pathRawdata = structure.pathRawdataTop;
  structure.pathResults = [structure.pathResults,'top/'];
  structure.rotAngle = structure.rotAngleTop;

  % Make subfolder if not yet present
  if ~exist(structure.pathResults, 'dir')
      mkdir(structure.pathResults);
  end

  [GJHAtop] = roughnessSurface(structure);

  %% Bottom
  % Overwrite fields for bottom side
  structure.pathRawdata = structure.pathRawdataBottom;
  structure.pathResults = [resultspath,'bottom/'];
  structure.rotAngle = structure.rotAngleBottom;

  % Make subfolder if not yet present
  if ~exist(structure.pathResults, 'dir')
      mkdir(structure.pathResults);
  end

  [GJHAbottom] = roughnessSurface(structure);

  structure.pathResults = resultspath;

  clear resultspath

  %% Joined analysis
  % Remove identical RowNames to combine both tables
  GJHAtop.Properties.RowNames={};
  GJHAbottom.Properties.RowNames={};

  % Combine values of both surfaces assuming self-affinity or at least a
  % strong similarity between both. Check the results and the provided
  % difference of each value provided therein.
  results = [GJHAtop; GJHAbottom];
  clear GJHAtop GJHAbottom

  % Calculate Difference
  results{7,:} = results{1,:} - results{4,:};
  results{8,:} = results{2,:} - results{5,:};

  % Calculate Mean
  results{9,:} = (results{1,:} + results{4,:}) * 0.5;
  results{10,:}= (results{2,:} + results{5,:}) * 0.5;

  % Mean Anisotropy
  results{11,:}= results{9,:} ./ results{10,:};

  % Update RowNames
  results.Properties.RowNames={'dim1_top', 'dim2_top', 'Anisotropy_top',...
                   'dim1_bottom', 'dim2_bottom', 'Anisotropy_bottom',...
                   'diff_dim1', 'diff_dim2',...
                   'Mean_dim1', 'Mean_dim2','Anisotropy'};

  % export table
  writetable(results,strcat(structure.pathResults,'results.csv'), ...
    'WriteRowNames',true)

  %% Aperture analysis
  calculateAperture(structure);
  
  %% Save workspace
  save([structure.pathResults, 'workspace'],'structure','results')

end
