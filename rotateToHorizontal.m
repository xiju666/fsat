%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function rotates provided 2D surface data by some degree provided
% through the metadata (rotAngle) to align the surface data vertical to the
% surface. This is necessary as many surface scanned data is not perfectly
% aligned. This function generates a figure of the rotated rawdata called
% rawdata_rotated. To find the correct angle of rotation to provide in the
% metadata, an iterative process might be needed.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function data = rotateToHorizontal(surface)

  % Rotation to have a rectangle!
  coordT=[surface.data(:,1) surface.data(:,2)];
  R=[cosd(surface.rotAngle) -sind(surface.rotAngle); ...
    sind(surface.rotAngle) cosd(surface.rotAngle)];
  rotcoord=coordT*R;
  data(:,1) = rotcoord(:,1);
  data(:,2) = rotcoord(:,2);
  data(:,3) = surface.data(:,3);

  clear rotcoord coordT R

  if (strcmp(surface.figures,'yes'))
    figure()
    scatter(data(:,1), data(:,2),1,surface.data(:,3))
    shading interp
    cb = colorbar();
    axis equal
    axis tight
    xlabel(['x-direction [', surface.units,']'])
    ylabel(['y-direction [', surface.units,']'])
    cb.Label.String = strcat('height [', surface.units,']');
    cb.Label.FontSize = 12;
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [surface.pathResults,'rawdata_rotated.',...
      surface.figfileformat], 'Resolution', 300)
  end
  close all

end
