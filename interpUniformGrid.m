%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function interpolats the provided data onto a uniform grid with a
% spacing as provided in the metadata.
% A figure of the interpolated values on the uniform grid is printed as 
% interpUniform. For a single profile line, it might be that the original
% data and the interpolated values perfectly overlap.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function interpdata = interpUniformGrid(structure)

  if (size(structure.data,2)>2)
    interpdata = griddata(structure.data(:,1),structure.data(:,2),...
        structure.data(:,3),structure.xUniform,structure.yUniform);
  else
    interpdata = interp1(structure.data(:,1),structure.data(:,2),...
        structure.xUniform,'linear','extrap');
  end
    
  if (structure.figures)
    figure()
    if (size(structure.data,2)>2)
      pcolor(structure.xUniform,structure.yUniform,interpdata);
      shading interp;
      axis equal;
      cb = colorbar();
      axis tight
      xlabel(['x-direction [', structure.units,']'])
      ylabel(['y-direction [', structure.units,']'])
      cb.Label.String = 'height [mm]';
      cb.Label.FontSize = 12;
    else
      plot(structure.xUniform, interpdata, '-k')
      hold on
      plot(structure.data(:,1), structure.data(:,2),'--k')
      hold off
      legend('uniform interp.','raw data');
      axis tight
      xlabel(['x-direction [', structure.units,']'])
      ylabel(['height [', structure.units,']'])
    end
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'interpolatedUniform.',...
                structure.figfileformat], 'Resolution', 300)
  end

end
