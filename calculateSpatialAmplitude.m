%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the spatial and amplitude paramters of an
% arbitrary number of profiles based on the derivation from a mean line of
% each individual profile.
% IMPORTANT: Note the difference between the values "relHeight" and
% "devFromMean" for a surface. For a single profile, those are equal but
% for a surface the mean line of each individual profile, in which the
% surface is splitted, can diver as well as in the different directions. 
%
% The function calculates several amplitude and spatial roughness 
% parameters such as zero-crossing density, peak density, mean 
% curvature and mean slope and several more. If more than one profile is
% given it also calculates the statistical distribution of those
% parameters generates a boxplot called dimX_gaussian_boxplots.
%
% Values are calculated based on the equations given in 
% Gadelmawla, E. S., Koura, M. M., Maksoud, T. M. A., Elewa, I. M., & 
% Soliman, H. H. (2002). Roughness parameters. Journal of Materials 
% Processing Technology, 123(1), 133–145. 
% https://doi.org/10.1016/S0924-0136(02)00060-2
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [result] = calculateSpatialAmplitude(structure,dim)

  %% Following equations are taken from Gadelmawla et al. (2002)
  % highest - lowest peak distance
  Rt = max(structure.devFromMean,[],dim,'omitnan') - ...
    min(structure.devFromMean,[],dim,'omitnan');

  % highest peak - mean line distance
  Rp = max(structure.devFromMean,[],dim,'omitnan');

  % lowest peak - mean line distance
  Rv = min(structure.devFromMean,[],dim,'omitnan');

  % 5 peak distance
  Rz = abs(mean( maxk(structure.devFromMean,5,dim),dim,'omitnan' ) ) - ...
       abs(mean( mink(structure.devFromMean,5,dim),dim,'omitnan' ) );

  % mean slope
  Ms = mean(diff(structure.devFromMean,1,dim),dim,'omitnan') ...
    / structure.spacing;
    
  % profile angularity
  thetap = atan(abs(Ms));

  % mean curvature
  if (dim == 1)
    z2nd = 2.0 * structure.devFromMean(2:end-1,:) - ...
                 structure.devFromMean(1:end-2,:) - ...
                 structure.devFromMean(3:end,:);
  else
    z2nd = 2.0 * structure.devFromMean(:,2:end-1) - ...
                 structure.devFromMean(:,1:end-2) - ...
                 structure.devFromMean(:,3:end);
  end
  Mc = mean(z2nd,dim,'omitnan') / structure.spacing^2;

  % local max density
  Np = sum(islocalmax(structure.devFromMean,dim),dim,'omitnan') ./ ...
    ( (sum(~isnan(structure.devFromMean),dim)-1) * structure.spacing );

  % density of zero crossings
  N0 = sum(abs(diff(sign(structure.devFromMean),1,dim)),dim,'omitnan') ... 
    ./ 2.0 ./ ...
    ( (sum(~isnan(structure.devFromMean),dim)-1) * structure.spacing );

  % save as structure to pass back to calling routine
  result.meanRt = mean(Rt,'omitnan');
  result.minRt = min(Rt,[],'omitnan');
  result.maxRt = max(Rt,[],'omitnan');
  result.stdRt = std(Rt,'omitnan');
  
  result.meanRp = mean(Rp,'omitnan');
  result.minRp = min(Rp,[],'omitnan');
  result.maxRp = max(Rp,[],'omitnan');
  result.stdRp = std(Rp,'omitnan');
  
  result.meanRv = mean(Rv,'omitnan');
  result.minRv = min(Rv,[],'omitnan');
  result.maxRv = max(Rv,[],'omitnan');
  result.stdRv = std(Rv,'omitnan');
  
  result.meanRz = mean(Rz,'omitnan');
  result.minRz = min(Rz,[],'omitnan');
  result.maxRz = max(Rz,[],'omitnan');
  result.stdRz = std(Rz,'omitnan');
  
  result.meanMs = mean(Ms,'omitnan');
  result.minMs = min(Ms,[],'omitnan');
  result.maxMs = max(Ms,[],'omitnan');
  result.stdMs = std(Ms,'omitnan');
  
  result.meanThetap = mean(thetap,'omitnan');
  result.minThetap = min(thetap,[],'omitnan');
  result.maxThetap = max(thetap,[],'omitnan');
  result.stdThetap = std(thetap,'omitnan');
  
  result.meanMc = mean(Mc,'omitnan');
  result.minMc = min(Mc,[],'omitnan');
  result.maxMc = max(Mc,[],'omitnan');
  result.stdMc = std(Mc,'omitnan');
  
  result.meanNp = mean(Np,'omitnan');
  result.minNp = min(Np,[],'omitnan');
  result.maxNp = max(Np,[],'omitnan');
  result.stdNp = std(Np,'omitnan');
  
  result.meanN0 = mean(N0,'omitnan');
  result.minN0 = min(N0,[],'omitnan');
  result.maxN0 = max(N0,[],'omitnan');
  result.stdN0 = std(N0,'omitnan');

  meanDz = mean(abs(structure.devFromMean),dim,'omitnan');
  if (strcmp(structure.figures,'yes') && max(size(meanDz)) > 1)
    idx = ~isnan(meanDz);
    figure()
    datain = reshape([Rt(idx), Rp(idx), Rv(idx), Rz(idx)], ...
        [length(Rt(idx)),4]);
    datana = {'$R_t$', '$R_p$','$R_v$','$R_z$'};
    boxplot(datain, 'Labels',datana);
    bp = gca;
    bp.XAxis.TickLabelInterpreter = 'latex';
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'dim',num2str(dim), ...
        '_amplitude_boxplots.',structure.figfileformat], 'Resolution', 300)
    
    figure()
    datain = reshape([Ms(idx), thetap(idx), Mc(idx)], ...
        [length(Ms(idx)),3]);
    datana = {'$M_s$', '$\theta_p$','$M_c$'};
    boxplot(datain, 'Labels',datana);
    bp = gca;
    bp.XAxis.TickLabelInterpreter = 'latex';
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'dim',num2str(dim), ...
        '_spatialChange_boxplots.',structure.figfileformat], ...
        'Resolution', 300)
      
    figure()
    datain = reshape([Np(idx), N0(idx)], ...
        [length(Np(idx)),2]);
    datana = {'$N_p$', '$N_0$'};
    boxplot(datain, 'Labels',datana);
    bp = gca;
    bp.XAxis.TickLabelInterpreter = 'latex';
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'dim',num2str(dim), ...
        '_spatialDensity_boxplots.',structure.figfileformat], ...
        'Resolution', 300)
  end
end
