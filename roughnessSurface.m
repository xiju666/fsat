%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the complete set of parameters to describe the
% roughness of a surface implemented in the FSA toolbox. It further handles
% the import of the rawdata and preprocessing such as rotation to the
% horizontal, adjusting the coordinate system, cutting the boundaries,
% interpolating a smooth plane to remove macroscopic tortuosity, and
% interpolates the rawdata onto a regular horizontal grid for further
% analysis.
% Calculation of roughness parameters is done by calling the corresponding
% functions (calculateGaussian, calculateJRC, calculateHurstExp,
% calculateSurfaceMeasures). The results of each function are collected in
% a single csv output file saved in the directory specified in the metadata
% (pathResults) and is named results.csv.
% This function further generates one figure showing the interpolated,
% relative height data on which the subsequent analysis is based.
% The generated file is named as:
% - relativeHeight
% For future analysis and modeling, the workspace is saved in a mat-file.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [results] = roughnessSurface(structure)

  structure.data = importRawdata(structure);

  structure.data = rotateToHorizontal(structure);

  structure.data = setToZero(structure);

  x0 = structure.cutoffx0;
  xend = structure.cutoffxend;
  y0 = structure.cutoffy0;
  yend = structure.cutoffyend;

  [structure.xUniform,structure.yUniform] = meshgrid(...
                                             x0:structure.spacing:xend,...
                                             y0:structure.spacing:yend);

  structure.zPlane = interpPlane(structure);

  structure.zUniform = interpUniformGrid(structure);

  %% Generate new relative z-data
  structure.relHeight = structure.zUniform - structure.zPlane;
  if (strcmp(structure.figures,'yes'))
    figure()
    pcolor(structure.xUniform,structure.yUniform,structure.relHeight)
    cb=colorbar();
    shading interp
    axis equal
    axis tight
    xlabel(['x-direction [', structure.units,']'])
    ylabel(['y-direction [', structure.units,']'])
    cb.Label.String = strcat('height [', structure.units,']');
    cb.Label.FontSize = 12;
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'relativeHeight.',...
      structure.figfileformat], 'Resolution', 300)
  end

  %% Calculate relative height statistics
  statistics = calculateRelHeightStatistics(structure);
  
  %% Roughness Analysis for each profile line in x-direction
  % x-direction: columns of z -> 2nd index but first dimension in matlab
  % z=z(1,:);  
  for dim=1:2
    % Calculate mean height along profile
    structure.meanHeight = mean(structure.relHeight,dim,'omitnan');

    % Calculate derivation from mean height
    % NOTE: The mean height is individually calculated for each profile. It
    % is therefore not the same for the whole surface and can also diver in
    % the various directions. For a surface, there are substantial
    % differences in the conception of the relative height (reference here
    % is the interpolated plane of the whole surface) and the derivation
    % from the mean height (reference here is the mean height of each
    % individual profile).
    structure.devFromMean = structure.relHeight - structure.meanHeight;

    spatialAmplitude(dim) = calculateSpatialAmplitude(structure,dim);

    %% Joint Rougness Coefficient
    jrc(dim) = calculateJRC(structure,dim);

    %% Fractal analysis
    hurst(dim) = calculateHurstExp(structure,dim);
    close all
  end

  %% Area measures
  area(1) = calculateSurfaceMeasures(structure);
  % Set dim 2 equals dim 1 for parallelism with gauss jrc and hurst
  statistics(2) = statistics(1);
  area(2) = area(1);

  S = struct2table(statistics);
  G = struct2table(spatialAmplitude);
  J = struct2table(jrc);
  H = struct2table(hurst);
  A = struct2table(area);

  results = [S,G,J,H,A];

  % Determine Anisotropy from horizontal to vertical
  results{3,:} = results{1,:} ./ results{2,:};
  results.Properties.RowNames={'dim1', 'dim2', 'Anisotropie'};

  % export table
  writetable(results,strcat(structure.pathResults,'results.csv'),...
    'WriteRowNames',true);

  % close all figures
  close all;

  %% save workspace
  clear S G J H A hurst area jrc spatialAmplitude statistics cb dim
  save([structure.pathResults,'workspace'],'structure','results');
  
  %% Anisotropy analysis in detail
  if (structure.angleStep > 0.0)
    calculateAnisotropy(structure)
  end

end
