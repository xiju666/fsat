%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function reads the x-y(-z) file as specified in the metadata
% (pathRawdata) and generates the structure.data field to store the
% rawdata. The rawdata is further visualized either as a scatter plot (2d
% surface) or a line plot (1d profile). The generated figure is saved as
% rawdata.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function data = importRawdata(surface)

  % Set up the import options
  if (surface.case == 'b' || surface.case == 'd')
    opts = delimitedTextImportOptions("NumVariables", 3);
    opts.VariableNames = ["x", "y", "z"];
    opts.VariableTypes = ["double", "double", "double"];
  else
    opts = delimitedTextImportOptions("NumVariables", 2);
    opts.VariableNames = ["x", "z"];
    opts.VariableTypes = ["double", "double"];
  end

  % Specify range and delimiter
  opts.DataLines = [1, Inf];
  opts.Delimiter = surface.delimiter;

  % Specify column names and types
  opts.ExtraColumnsRule = "ignore";
  opts.EmptyLineRule = "read";
  opts.ConsecutiveDelimitersRule = "join";
  opts.LeadingDelimitersRule = "ignore";

  % Import the data
  XYZ = readtable(surface.pathRawdata, opts);

  % Clear temporary variables
  clear opts

  %% Output
  if (surface.case == 'b' || surface.case == 'd')
    data = [XYZ.x XYZ.y XYZ.z];
  else
    data = [XYZ.x XYZ.z];
  end

  %% first visualization
  if (strcmp(surface.figures,'yes'))
    figure()
    if (strcmp(surface.case,'b') || strcmp(surface.case,'d'))
      scatter(XYZ.x, XYZ.y,1,XYZ.z)
      cb = colorbar();
      axis equal
      axis tight
      xlabel(['x-direction [', surface.units,']'])
      ylabel(['y-direction [', surface.units,']'])
      cb.Label.String = strcat('height [', surface.units,']');
      cb.Label.FontSize = 12;
    else
      plot(XYZ.x, XYZ.z,'-k')
      axis tight
      xlabel(['x-direction [', surface.units,']'])
      ylabel(['z-direction [', surface.units,']'])
    end
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [surface.pathResults,'rawdata.',...
      surface.figfileformat], 'Resolution', 300)
  end
  close all

end
