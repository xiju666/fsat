%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the Hurst Exponent and the fractal dimension
% based on the concept outlined in:
% Schmittbuhl, J., Gentier, S., & Roux, S. (1993). Field measurements of 
% the roughness of fault surfaces. Geophysical Research Letters, 20(8), 
% 639–641. https://doi.org/10.1029/93GL00170
%
% In this concept, the profile is split into bands, which width is varying
% from iteration to iteration. For each bandwith the mean value amplitude
% difference between the highest peak and the lowest valey is determined.
% As the bandwith increases, this value also decreases. In a log-log plot a
% linear trend can be detected over a range of bandwiths. The inclination
% of this line in the log-log plot is the Hurst exponent, which is linearly
% connected to the fractal dimension D by D = E - H with the euclidean 
% dimension E.
%
% If more than one profile is passed to this function, the statistical
% distribution of the determined parameters is also calculated and
% visualized in a boxplot saved as dimX_Hurst_boxplot.
% Further, this function prints a figure showing the determined linear fit
% in the log-log plot as well, either for the single profile or for the
% highest and lowest Hurst exponent determined. The figure is saved as:
% dimX_Hurst_fit.
% For determining the Hurst exponent, the linear interval of the scaling
% needs to be identified. This can be done through the metainformations
% HurstStart and HurstEnd. An iterative procedure might be optimal: First
% plotting the values for the various bandwidths (figure dimX_Hurst_fit)
% and then adjusting the values HurstStart and HurstEnd accordingly.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [result] = calculateHurstExp(structure, dim)

  z = structure.devFromMean;

  % determine maximum number of points along the profile
  L = sum(~isnan(z),dim);

  % maximum window depth should not be larger than half the profile
  % length measured in points of the profile
  Dmax = floor(L/2);
  
  % Determining Hurst and its Amplitude parameter for each profile
  Hurst = zeros(1,length(Dmax));
  Amplitude = zeros(1,length(Dmax));

  % For each profile
  for ip=1:length(L)
    if (Dmax(ip)>0)
      % Define number of bands and bandwidth
      Delta{ip} = floor( linspace( 2, Dmax(ip), floor(Dmax(ip)/5) ) );

      % Initiate measure for each bandwidth
      d{ip} = zeros(1,length(Delta{ip}));

      % iterate through all bandwidths
      for i=1:1:length(Delta{ip})
        tempH = [];
        k=0;
        % iterate through each band with given bandwidth
        for j=0:floor(0.5*Delta{ip}(i)):L(ip)-Delta{ip}(i)-1
          k=k+1;
          if dim==1
            %determine first non-NaN value in each profile
            idx = find(~isnan(z(:,ip)),1);
            % temporary variable to save height difference within each band
            tempH(k) = max(z(idx+j:idx+j+Delta{ip}(i),ip),[],'omitnan')-...
                       min(z(idx+j:idx+j+Delta{ip}(i),ip),[],'omitnan');
          else
            %determine first non-NaN value in each profile
            idx = find(~isnan(z(ip,:)));
            % temporary variable to save height difference within each band
            tempH(k) = max(z(ip,idx+j:idx+j+Delta{ip}(i)),[],'omitnan')-...
                       min(z(ip,idx+j:idx+j+Delta{ip}(i)),[],'omitnan');
          end
        end
        % mean value of all bands
        d{ip}(i) = mean(tempH,'omitnan');
      end

      %% Fitting
      y = d{ip}(log(Delta{ip})>structure.HurstStart);
      x = Delta{ip}(log(Delta{ip})>structure.HurstStart);
      y = y(log(x)<structure.HurstEnd);
      x = x(log(x)<structure.HurstEnd);

      fit1 = polyfit(log(x),log(y),1);
      Hurst(ip) = fit1(1);
      Amplitude(ip) = exp(fit1(2));
    end
  end
  
  Hurst = nonzeros(Hurst);
  Amplitude = nonzeros(Amplitude);
  result.meanHurst = mean(Hurst);
  result.minHurst = min(Hurst);
  result.maxHurst = max(Hurst);
  result.stdHurst = std(Hurst);
  result.meanAHurst = mean(Amplitude);
  result.minAHurst = min(Amplitude);
  result.maxAHurst = max(Amplitude);
  result.stdAHurst = std(Amplitude);
  
  if (structure.case == 'a' || structure.case == 'c')
    E = 2;
  else
    E = 3;
  end
  result.meanD = E - mean(Hurst);
  result.minD = min(E - Hurst);
  result.maxD = max(E - Hurst);
  result.stdD = std(E - Hurst);

  if (strcmp(structure.figures,'yes'))
    figure()
    if (max(size(Hurst)) > 1)
      boxplot(Hurst,'label',' ');
      ylabel('Hurst Exponent')
      set(gcf, 'Color', 'w');
      set(gcf, 'Position', [400 400 350 550]);
      exportgraphics(gca, [structure.pathResults,'dim',num2str(dim),...
          '_Hurst_boxplot.',structure.figfileformat], 'Resolution', 300)

      figure()
      [M1, I1] = min(Hurst);
      [M2, I2] = max(Hurst);
      p1=plot(log(Delta{I1}), ...
        Hurst(I1)*log(Delta{I1})+log(Amplitude(I1)), '-r');
      hold on
      p2=plot(log(Delta{I1}), ...
        Hurst(I2)*log(Delta{I1})+log(Amplitude(I2)), '-k');
      plot(log(Delta{I1}), log(d{I1}),'+r')
      plot(log(Delta{I2}), log(d{I2}),'+k')
      legend([p1 p2], ['Hurst=',num2str(M1)],['Hurst=',num2str(M2)],...
          'Location','southeast')
      ylim([min(min(log(d{I2})),min(log(d{I1})))-0.1, ...
            max(max(log(d{I1})),max(log(d{I2})))+0.1])
      hold off
    else
      p1 = plot(log(Delta{1}), Hurst*log(Delta{1})+log(Amplitude), '-k');
      hold on
      plot(log(Delta{1}), log(d{1}),'+r');
      hold off
      legend(p1,['Hurst=',num2str(Hurst)],'Location','southeast')
    end
    xlabel(['log(Bandwidth [', structure.units,'])'])
    ylabel(['log(Distance high-low per band [', structure.units,'])'])
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,'dim',num2str(dim),...
        '_Hurst_fit.',structure.figfileformat], 'Resolution', 300)
  end
end
