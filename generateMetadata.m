%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file is used to provide the required metadata for each sample or
% batch of samples to be processed by the FSA toolbox.
%
% A sample can either consist of
% a) a single profile line (x-z data)
% b) a singe surface (x-y-z data)
% c) two profile lines forming a vertical cut through a fracture (2 x-z
% datasets)
% d) two surfaces forming a three-dimensional fracture based on xyz files
% of each of the 2d surface scans (2 x-y-z datasets)
%
% The metadata is stored in a structure called "sample" in matlab. All 
% samples require:
% - a specifier which case is used here ('a', 'b', 'c', 'd')
% - a flag if figures should be generated or not ('no', 'yes')
% - a specifier for the fileformat for figures (if figures are used)
% - a name attribute sample(i).name, 
% - a path attribute to the raw data file sample(i).path, 
% - the delimiter used in the source file
% - the physical units used for the values in the source file
% - a path where results are supposed to be saved,
% - a regular spacing value because most scanned data is not uniformly 
% spaced and might contain holes. Therefore, FSAT interpolates the rawdata
% to a regularly spaced grid for 2d surfaces. The spacing should be equal
% to or larger than the mean point distance of the rawdata. In general, 
% larger values result in smoother surfaces.
% - possible cutoffs. Those cutoffs are measured in the units of the 
% provided data file and provide start and max. length. They 
% are often required as data obtained from surface scans is error prawn at
% the boundaries. Both are measured assuming a start of the original raw 
% data at zero!
% - upper and lower limit of the bandwith used to calculate the Hurst
% exponent.
%
% Depending on the chosen case, additional metadata needs to be provided.
%
% case a) requires cutoff values along the profile line (x) value.
% Exemplary metadata could look like
% sample(1).case = 'a';
% sample(1).figures = 'yes';
% sample(1).figfileformat = 'png';
% sample(1).name = 'profile';
% sample(1).pathRawData = '../data/profile.csv';
% sample(1).pathResults = '../results/';
% sample(1).delimiter = ' ';
% sample(1).units = 'mm';
% sample(1).cutoffx0 = 0.0;
% sample(1).cutoffxend = 95.0;
% sample(1).spacing = 0.2;
% sample(1).HurstStart = 0.0;
% sample(1).HurstEnd = 4.0;
%
% case b) requires additionally to case a)
% - cutoffs in the second dimension
% - a rotation angle in degrees around the origin for the case that the 
% provided data is not properly rectangular aligned. To find this rotation
% angle, one can call the function importXYZFiles(filename) of this toolbox
% to generate a plot of the rawdata and to estimate the required rotation
% angle. To test for the effect of the rotation angle, one can call the
% function rotateToHorizontal(data, rotAngle). See documentation of those
% functions for further instructions.
% - an angle stepsize in degree, in which anisotropy is evaluated can be given. A
% negative value, will result in a skip of the anisotropy evaluation.
% Exemplary metadata could look like
% sample(1).case = 'b';
% sample(1).figures = 'yes';
% sample(1).figfileformat = 'png';
% sample(1).name = 'surface';
% sample(1).pathRawdata = '../data/surface.csv';
% sample(1).pathResults = '../results/';
% sample(1).delimiter = ' ';
% sample(1).units = 'mm';
% sample(1).cutoffx0 = 3.0;
% sample(1).cutoffxend = 97.0;
% sample(1).cutoffy0 = 3.0;
% sample(1).cutoffyend = 147.0;
% sample(1).rotAngle = -2.4;
% sample(1).spacing = 0.2;
% sample(1).HurstStart = 0.0;
% sample(1).HurstEnd = 4.0;
% sample(1).angleStep = 22.5;
%
% case c) requires similar attributes than case a) but two file names.
% Cutoff values have to be assigned to both sides equally.
% It further requires a mean aperture to match both surfaces sufficiently
% close together.
% Exemplary metadata could look like
% sample(1).case = 'c';
% sample(1).figures = 'yes';
% sample(1).figfileformat = 'png';
% sample(1).name = '2d_fracture';
% sample(1).pathRawdataTop = '../data/profile_top.csv';
% sample(1).pathRawdataBottom = '../data/profile_bottom.csv';
% sample(1).pathResults = '../results/';
% sample(1).delimiter = ' ';
% sample(1).units = 'mm';
% sample(1).cutoffx0 = 0.0;
% sample(1).cutoffxend = 95.0;
% sample(1).spacing = 0.2;
% sample(1).HurstStart = 0.0;
% sample(1).HurstEnd = 4.0;
% sample(1).aperture = 3.0;
%
% case d) is analog to case b) but also requires two file names as well as
% two rotation angles because they might differ if both surfaces were
% scanned separately.
% Similar to case c) cutoff values have to be assigned equally to both 
% sides, as well as the spacing for interpolation. Similarly, a mean 
% aperture needs to be defined.
% Exemplary metadata could look like
% sample(1).case = 'd';
% sample(1).figures = 'yes';
% sample(1).figfileformat = 'png';
% sample(1).name = '3d_fracture';
% sample(1).pathRawdataTop = '../data/surface_top.csv';
% sample(1).pathRawdataBottom = '../data/surface_bottom.csv';
% sample(1).pathResults = '../results/';
% sample(1).delimiter = ' ';
% sample(1).units = 'mm';
% sample(1).cutoffx0 = 3.0;
% sample(1).cutoffxend = 97.0;
% sample(1).cutoffy0 = 3.0;
% sample(1).cutoffyend = 147.0;
% sample(1).rotAngleTop = -2.4;
% sample(1).rotAngleBottom = -2.1;
% sample(1).spacing = 0.2;
% sample(1).HurstStart = 0.0;
% sample(1).HurstEnd = 4.0;
% sample(1).angleStep = 22.5;
% sample(1).aperture = 3.0;
%
% Batches of samples can be generated by increasing the number in the
% brackets. The next sample would be called by sample(2).
%
% The whole structure "sample" is saved in a mat file. The filename and its 
% path can be specified as user input. The filename/path needs to be passed
% to the main file analyzesample.m.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Fresh start
clear all
close all
clc

%% Specify path and name of file to store metadata
pathToMetadata = 'testdata.mat';

%% Generate meta-information for samples
sample(1).case = 'a';
sample(1).figures = 'yes';
sample(1).figfileformat = 'png';
sample(1).name = 'profile_test';
sample(1).pathRawdata = './test_data/profile_top.csv';
sample(1).pathResults = './test_results/profile/';
sample(1).delimiter = ',';
sample(1).units = 'mm';
sample(1).cutoffx0 = 0.0;
sample(1).cutoffxend = 95.0;
sample(1).spacing = 0.2;
sample(1).HurstStart = 0;
sample(1).HurstEnd = 4.0;

sample(2).case = 'b';
sample(2).figures = 'yes';
sample(2).figfileformat = 'png';
sample(2).name = 'surface_test';
sample(2).pathRawdata = './test_data/surface_top.xyz';
sample(2).pathResults = './test_results/surface/';
sample(2).delimiter = ' ';
sample(2).units = 'mm';
sample(2).cutoffx0 = 3.0;
sample(2).cutoffxend = 97.0;
sample(2).cutoffy0 = 3.0;
sample(2).cutoffyend = 147.0;
sample(2).rotAngle = -2.4;
sample(2).spacing = 0.2;
sample(2).HurstStart = 0;
sample(2).HurstEnd = 4.0;
sample(2).angleStep = 22.5;

sample(3).case = 'c';
sample(3).figures = 'yes';
sample(3).figfileformat = 'png';
sample(3).name = 'fracture_2D_test';
sample(3).pathRawdataTop = './test_data/profile_top.csv';
sample(3).pathRawdataBottom = './test_data/profile_bottom.csv';
sample(3).pathResults = './test_results/fracture_2d/';
sample(3).delimiter = ',';
sample(3).units = 'mm';
sample(3).cutoffx0 = 3.0;
sample(3).cutoffxend = 94.0;
sample(3).spacing = 0.2;
sample(3).HurstStart = 0;
sample(3).HurstEnd = 4.0;
sample(3).aperture = 1.0;

sample(4).case = 'd';
sample(4).figures = 'yes';
sample(4).figfileformat = 'png';
sample(4).name = 'fracture_3D_test';
sample(4).pathRawdataTop = './test_data/surface_top.xyz';
sample(4).pathRawdataBottom = './test_data/surface_bottom.xyz';
sample(4).pathResults = './test_results/fracture_3d/';
sample(4).delimiter = ' ';
sample(4).units = 'mm';
sample(4).cutoffx0 = 3.0;
sample(4).cutoffxend = 97.0;
sample(4).cutoffy0 = 3.0;
sample(4).cutoffyend = 147.0;
sample(4).rotAngleTop = -2.4;
sample(4).rotAngleBottom = -2.1;
sample(4).spacing = 0.2;
sample(4).HurstStart = 0;
sample(4).HurstEnd = 4.0;
sample(4).angleStep = 22.5;
sample(4).aperture = 0.120;

sample(5).case = 'd';
sample(5).figures = 'yes';
sample(5).figfileformat = 'png';
sample(5).name = 'circle_test';
sample(5).pathRawdataTop = './test_data/circle_top.xyz';
sample(5).pathRawdataBottom = './test_data/circle_bottom.xyz';
sample(5).pathResults = './test_results/circle_3d/';
sample(5).delimiter = ',';
sample(5).units = 'mm';
sample(5).cutoffx0 = 0.0;
sample(5).cutoffxend = 80.0;
sample(5).cutoffy0 = 0.0;
sample(5).cutoffyend = 80.0;
sample(5).rotAngleTop = -2.4;
sample(5).rotAngleBottom = -2.1;
sample(5).spacing = 0.2;
sample(5).HurstStart = 0;
sample(5).HurstEnd = 4.0;
sample(5).angleStep = 22.5;
sample(5).aperture = 0.120;
%%
save(pathToMetadata, 'sample')
clear pathToMetadata
