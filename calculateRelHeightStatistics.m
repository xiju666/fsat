%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function calculates the statistical parameters of a profile or
% surface based on its relative height to a mean line or surface.
% The calculated parameters include standard deviation, skewness, kurtosis 
% and several more. 
% If more than one profile is given it also calculates the statistical 
% distribution of those parameters and generates a boxplot called 
% statistics_boxplots.
% The distance from the mean line is also visualized in a histogram called
% statistics_histogram_relHeight.
%
% Values are calculated based on the equations given in Bhushan, B. (2000).
% Surface roughness analysis and measurement techniques. (B. Bhushan, Ed.),
% Modern Tribology Handbook: Volume One: Principles of Tribology. Boca 
% Raton, Florida, USA: CRC Press LLC.
%
% Copyright (c) 2020, Thomas Heinze
%
% This file was written by Thomas Heinze, Bochum, Germany, 2020 as part of
% the FSA Toolbox.
% Please cite as: 
% Thomas Heinze, Sascha Frank & Stefan Wohnlich (2021). 
% FSAT – A fracture surface analysis toolbox in MATLAB to compare 2D and 
% 3D surface measures. Computers and Geotechnics, Volume 132, 103997. 
% doi: 10.1016/j.compgeo.2020.103997. 
% https://doi.org/10.1016/j.compgeo.2020.103997
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [result] = calculateRelHeightStatistics(structure)

  % Calculate mean derivation from mean height
  meanDz = mean(abs(structure.relHeight),'omitnan');

  if (strcmp(structure.figures,'yes'))
    figure()
    histogram(structure.relHeight,100)
    xlabel(['Distance from mean line [', structure.units,']'])
    ylabel('Occurrence')
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,...
        'statistics_histogram_relHeight.',structure.figfileformat],...
        'Resolution', 300)
  end

  % Calculate variance of vertical derivation
  varDz = var(structure.relHeight,0,'omitnan');

  % Calculate standard variance of vertical derivation
  stdDz = std(structure.relHeight,0,'omitnan');

  % Calculate Skewness (Bhushan, 2000; equ. 2.5)
  sk = mean(structure.relHeight.*structure.relHeight.*...
      structure.relHeight,'omitnan') ./ stdDz.^3;

  % Calculate Kurtosis (Bhushan, 2000; equ. 2.6)
  kurt = mean(structure.relHeight.^4,'omitnan') ./ stdDz.^4;
  
  % save as structure to pass back to calling routine
  result.meanDz = mean(meanDz,'omitnan');
  result.VarDz = mean(varDz,'omitnan');
  result.StdDz = mean(stdDz,'omitnan');
  
  result.meanSkew = mean(sk,'omitnan');
  result.minSkew = min(sk,[],'omitnan');
  result.maxSkew = max(sk,[],'omitnan');
  result.stdSkew = std(sk,[],'omitnan');
  
  result.meanKurt = mean(kurt,'omitnan');
  result.minKurt = min(kurt,[],'omitnan');
  result.maxKurt = max(kurt,[],'omitnan');
  result.stdKurt = std(kurt,'omitnan');
  
  if (strcmp(structure.figures,'yes') && max(size(meanDz)) > 1)
    idx = ~isnan(meanDz);
    figure()
    datain = reshape([meanDz(idx), varDz(idx), stdDz(idx), sk(idx), ...
      kurt(idx)], [length(kurt(idx)),5]);
    datana = {'Mean', 'Var','std','Skewness','Kurtosis'};
    boxplot(datain, 'Labels',datana);
    bp = gca;
    bp.XAxis.TickLabelInterpreter = 'latex';
    set(gcf, 'Color', 'w');
    set(gcf, 'Position', [400 400 750 550]);
    exportgraphics(gca, [structure.pathResults,...
        'statistics_boxplots.',structure.figfileformat], 'Resolution', 300)
  end
end